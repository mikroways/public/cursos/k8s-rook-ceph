# Ejemplos

----

Todos los ejemplos están en el mismo repositorio que ésta presentación:

## https://bit.ly/2Vw3vlY

Cada aplicación tiene un directorio en:

### [src/examples/gitops](https://gitlab.com/mikroways-public/cursos/k8s-argocd/-/tree/master/src/examples/gitops)

----


## Aplicación usando kustomize

### [src/examples/gitops/kustomize-hello-world](https://gitlab.com/mikroways-public/cursos/k8s-argocd/-/tree/master/src/examples/gitops/kustomize-hello-world)

* Es posible parametrizar:
  * Imágenes
  * Pre(Su)fijo de objetos
----
## Políticas de sincronización

* Automated
* Prune resources
* Self Heal

----

## Aplicación usando Helm

### [src/examples/gitops/helm-hello-world](https:////gitlab.com/mikroways-public/cursos/k8s-argocd/-/tree/master/src/examples/gitops/helm-hello-world)

Es posible parametrizar lo que hallamos determinado en el diseño de nuestro chart

<small><b>Es mucho más potente usar helm que kustomize</b></small>

> Observar en el ejemplo la dependencia entre el ConfigMap y el deployment

----

## Más de Argo...

* Aplicaciones de aplicaciones. Bootstrapping
  * Personalidades con TOC abstenerse
* Ejemplos reales
  * Una idea de cómo trabajar con proyectos
* Cómo buscar problemas
* Uso de argocd cli
* Uso de webhooks. **_Especial para los ansiosos_**

---

# ¿Preguntas?

### Envianos un email a contacto@mikroways.net
<!-- .element: class="fragment"-->

