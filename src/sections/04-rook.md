# Rook

<img class="main" height="180px" src="static/rook-logo.png" />
----
## ¿Qué es Rook?

Rook es un [operador de kubernetes](https://coreos.com/blog/introducing-operators.html)
que simplifica la compleja gestión de administrar storage distribuidos.

Para ello, Rook utiliza Custom Resource Deifinitions (CRD) y un controlador para
desplegar y manejar estos CRD, para por ejemplo gestionar clusters Ceph.

Automatiza las tareas de un administrador de storage: _despliegue, bootstrapping,
configuración, aprovisionamiento, escalado, actualización, migración,
recuperación ante desastres, monitoreo y gestión de recursos_.

----

## Soporta múltiples proveedores

* Ceph
* [EdgeFS](https://github.com/Nexenta/edgefs): Object Store (S3 compat).
* [CockroachDB](https://www.cockroachlabs.com/): DB SQL distribuida.
  * [Compatibilidad con PSQL](https://www.cockroachlabs.com/docs/stable/postgresql-compatibility.html)
* [Cassandra](https://cassandra.apache.org/)/[Scylla](https://www.scylladb.com/): DB NoSQL
* NFS: inicia un servidor NFS utilizando un PVC.
  * EL PVC podría ser rbd de Ceph, CephFS, AWS EBS, GCP Persistent Disk. 
* [Yugabyte DB](https://www.yugabyte.com/): DB SQL distribuida.
  * [Compatibilidad con PSQL](https://docs.yugabyte.com/latest/reference/drivers/)

----

## Instalación del operador rook-ceph

```
helm repo add rook-stable https://charts.rook.io/stable

helm install --name rook --namespace rook-ceph-system \
  rook-stable/rook-ceph
```

> Requiere k8s 1.11 o superior. Para el caso de ceph requiere instalar LVM si se
> utilizan dispositivos RAW. Kernel 4.17 o superior para que se fuercen los
> tamaños definidos en los PVC con CephFS. Si no se respeta, funcionarán sin
> limitarse el uso por el valor indicado en el PV.
----

## Crear un cluster Ceph

El cluster ceph puede utilizar un **directorio** del disco de cada nodo o
un **dispositivo de bloque** sin formatear presentado al nodo.

----

## Crear un cluster Ceph

CODE: resources/cluster.yaml yaml

----

## Configurar Ceph block provisioner

CODE: resources/ceph-block-pool.yaml yaml

<small>Crea un storage pool en Ceph usando CRD</small>

----
## Configurar CephFS provisioner

CODE: resources/cephfs.yaml yaml


<small>Crea un CephFS en Ceph usando CRD</small>

----

## ¿Cómo se usan los provisioners?

Definiendo `StorageClass` para cada uno

----

## StorageClass para Ceph blocks

CODE: resources/storage-class-ceph-block.yaml yaml

----

## StorageClass para CephFS

CODE: resources/storage-class-ceph-fs.yaml yaml

----

## Ejemplo de uso RBD

CODE: resources/pvc.yaml yaml

----

## Ejemplo de uso CephFS

CODE: resources/pvc-shared-fs.yaml yaml

----

## Dashboard

<img class="main" height="500px" src="static/ceph-dashboard.png" />
----

## Otras características destacables

* **RBD snapshots:** a partir de kubernetes 1.12+, esta característica es alfa.
  En la 1.17 ya beta. _Ver [tabla de feature
gates](https://kubernetes.io/docs/reference/command-line-tools-reference/feature-gates/#feature-gates-for-alpha-or-beta-features)_
* **Expansión dinámica de volúmenes:** requiere kuberntes 1.16+. Su uso es muy
  simple, porque cambiando el tamaño solicitado para un PVC, el PV se expande
  automáticamente.

----

## Rook Ceph Toolbox

Rook provee un contenedor con herramientas para la gestión y debug de ceph. Para
ello, ceph, provee un yaml para inciar este pod, y luego utilizarlo de la
siguiente forma:

```
kubectl -n rook-ceph exec -it \
  $(kubectl -n rook-ceph get pod -l "app=rook-ceph-tools" \
    -o jsonpath='{.items[0].metadata.name}') bash
```

----

## Uso del toolbox

```
ceph status
ceph osd status
ceph osd lspools
# El pool replicapool usado en el siguiente ejemplo surge del 
# CephBlockPool creado previamente
rbd ls -p replicapool
```

----
## Correspondencia CSI RBD

`kubectl get pv <PV-NAME> -o yaml`

CODE: resources/pv-output.yaml yaml 40

----

## Correspondencia CSI RBD

```
kubectl get pv <PV-NAME> -o jsonpath='{.spec.csi.volumeHandle}' \
  | cut -b38-
```
<small>
La salida del comando anterior, tiene el identificador de la imagen RBD en ceph.
<br />
En ceph, la imagen se llama <b>csi-vol-ID</b>
<small>

----

## Correspondencia CSI CephFS

`kubectl get pv <PV-NAME> -o yaml`

CODE: resources/pv-cephfs-output.yaml yaml 33-34

----

## Snapshots

* Rook Ceph permite integrar facilidades de k8s con el soporte de Ceph para
  realizar snapshots.
* Para ello, es necesario crear un `VolumeSnapshotClass` y luego un
  `VolumeSnapshot`
* Luego, será posible crear un PVC a partir de un snapshot

---

# ¿Preguntas?

### Envianos un email a contacto@mikroways.net
<!-- .element: class="fragment"-->

