## Presentación
----
### Sobre Mikroways

* Brindamos servicios de TI, específicamente relacionados a la infraestructura
  tecnológica de nuestros clientes.
* Diseñamos e implementamos soluciones a medida según las necesidades de cada
  cliente.
* Damos soporte para solucionar cualquier problemática que surja en el
  funcionamiento de la infraestructura tecnológica existente.
* Proveemos capacitación al personal de sistemas, adaptando los cursos a los
  requerimientos específicos.
----
## Nuestros servicios

* DevOps
* Monitoreo & Logs
* Docker & Kubernetes & Openshift
* Entrega y Despliegue contínuo. GitOps
* Cloud Computing
* Escalabilidad de aplicaciones web
* Capacitaciones

----
## Agenda

- Introducción: el problema
- Ceph
- Rook
- Rook Ceph
- Ejemplos
----

## Conocimientos previos ideales

- SysAdmin / Dev
- Servicios
- Contenedores / docker
- K8S
- JSON / YAML

----

## Recomendación

> La capacitación de **Introducción a Kubernetes** dictada por Mikroways es
> recomendada para un mejor entendimiento de este curso.
