# Ceph

<img class="main" height="180px" src="static/ceph-logo.png" />
----

## ¿Qué es Ceph?

Es un _Software Defined Storage_ open source que provee:
* **Object store** distribuido similar al servicio de AWS S3
* **Dispositivos de bloque** remoto (RBD).
* **Filesystem distribuido** (CephFS) POSIX.
  * Requiere un cliente de CephFS, pero en casos que no se dipone del cliente
    es posible exponer NFS/CIFS.

----
<!-- .slide: class="list medium" -->

## Componentes Ceph

* **Monitors (MON):** mantienen un mapa del cluster y controlan el quorum
   necesario para su funcionamiento. Autentican los diferentes daemons del cluster.
* **Object Store Devices (OSD):** almacenan los datos en el filesystem local.
  Generalmente un OSD utilizará un disco físico. Al menos 3 OSD son requeridos
  para gatantizar redundancia y HA.
* **Manager (MGR):** proveen métricas y monitoreo del estado del cluster. Entre
  otras cosas, se provee un dashboard que muestra el estado del cluster.
* **Metadata Server (MDS)** almacena metadatos para CephFS (Object Store y RBD
  no lo requieren). MDS permite la ejecución eficiente de comandos como `ls`,
  `find`, etc.

----
<!-- .slide: class="list medium" -->

## Cómo trabaja Ceph 

* Almacena datos como objetos dentro de storage pool lógicos.
* Usando el [algoritmo
  CRUSH](https://ceph.com/wp-content/uploads/2016/08/weil-crush-sc06.pdf)
_-Controlled Replication Under Scalable Hashing-_, se calcula qué OSD
almacenará el dato.
  * CRUSH ofrece al cluster la posibilidad de escalar, rebalancear y recuperarse
    dinámicamente.
* Independientemente del tipo de uso que demos a Ceph, siempre se almacenan
  datos como objetos. Es por ello que se dice que RADOS _-Reliable Autonomic
  Distributed Object Store-_ es la base de cualquier cluster Ceph.

----

# Ceph

<iframe width="560" height="315" src="https://www.youtube.com/embed/QBkH1g4DuKE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## [Casos de uso ](https://ceph.io/use-cases)


