# Introducción

----

## Contenedores y volúmenes

* Los contenedores se construyen sobre el principio de no tener estado: **stateless**.
* Pero los datos deben almacenarse y disponibilizarse para ciertos servicios:
  * Bases de datos
  * Uploads
* Es por ello, que los volúmenes surgen como recurso para preservar los datos
  ante el reinicio de contenedores.

----

# PVC

## Persistent Volume Claim

----

## Persistent Volume Claim

* Los PVCs son la forma recomendada para manejar **aplicaciones stateful** en
kubernetes.
* Un POD podrá entonces requerir conectarse a un volumen, considerando que el
  ciclo de vida del POD será independiente del ciclo de vida del volumen.
* Los PVC representan una solicitud de storage por un POD. Los PVC terminarán
  asociándose a un PV (Persisten Volume) que finalmente será montado sobre un
  POD.

----

# PV

## Persisten Volume

----
## Persisten Volume

* Representan los volúmenes que podrán ser creados:
  * Manualmente por un Administrador (humano)
  * Dinámicamente por un StorageClass
* Los PV no se relacionan a un pod particular, sino que tienen su propio ciclo
  de vida en un custer kubernetes.
* Los PV se asocian a un storage físico (volumen real) que es el que finalmente
  se presenta al nodo donde se correrá el POD que requiera usar un PV mediante
  un PVC.

----

# Los Problemas

----

## Dependencia entre volúmenes y nodos

* Un contenedor puede correr en cualquier nodo de un cluster.
* Si el contenedor se reinicia puede iniciarse en otro nodo del cluster.
* Existen diferentes tipos de volúmenes y dependiendo del tipo usado por el POD,
  será posible que el POD inicie en otro nodo o no. Por ejemplo, _hostPath y local_, 
  no soportan la migración de PODs.

<small>
<em>
Para entender bien todas las posibilidades, debe remitirse a los
<a href="https://kubernetes.io/docs/concepts/storage/volumes/#types-of-volumes">
tipos de volúmenes en kubernetes</a>
</em>
</small>
----
<!-- .slide: class="list medium" -->

## Modos de acceso al volumen

Un PV puede montarse en uno o más nodos dependiendo del tipo de volumen. Por
ello, kubernetes define los siguientes modos de acceso:

* **Read Write Once (RWO):** el volumen puede montarse RW por un único nodo.
* **Read Only Many (ROX):** el volumen puede montarse RO por varios nodos.
* **Read Write Many (RWX):** el volumen puede montarse RW por varios nodos.

> Un volumen podrá ser usado con un modo de acceso a la vez, incluso cuando
> soporte más de uno.

----

## Tablas de modos de acceso

<img class="main" height="500px" src="static/k8s-access-modes.png" />

