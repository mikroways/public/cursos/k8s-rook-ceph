---
title: Rook Ceph
theme: league
highlightTheme: vs2015
preprocessor: preproc.js
revealOptions:
  transition: 'slide'
---

# Rook Ceph

<div class="container" style="margin-top: 50px">
  <div class="col">
    <img class="main" height="180px" src="static/rook-logo.png" />
  </div>
  <div class="col">
    <img class="main" height="150px" src="static/k8s.png" />
  </div>
  <div class="col">
    <img class="main" height="180px" src="static/ceph-logo.png" />
  </div>
</div>
<div>
  <img class="main" style="padding-top:50px" height="60px" src="static/logo-nombre-transparente-blanco.png" />
</div>

https://bit.ly/2AEn0AE

---

FILE: 01-presentacion.md

---

FILE: 02-introduccion.md

---

FILE: 03-ceph.md

---

FILE: 04-rook.md

